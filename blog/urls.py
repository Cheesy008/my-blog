from django.urls import path
from .views import *


urlpatterns = [
    path('', posts_list, name="post_lists_url"),
    path('post/create/', PostCreate.as_view(), name="post_create_url"),
    path('post/<str:slug>/', post_detail, name="post_details_url"),
    path('post/<str:slug>/edit/', PostEdit.as_view(), name="post_edit_url"),
    path('tags/', tags_list, name="tags_list_url"),
    path('tag/create/', TagCreate.as_view(), name="tag_create_url"),
    path('tag/<str:slug>/', TagDetail.as_view(), name="tag_detail_url"),
    path('tag/<str:slug>/edit/', TagEdit.as_view(), name="tag_edit_url"),
    path('tag/<str:slug>/delete/',TagDelete.as_view(), name="tag_delete_url"),
    path('post/<str:slug>/delete/',PostDelete.as_view(), name="post_delete_url"),
]
